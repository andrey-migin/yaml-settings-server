using NUnit.Framework;

namespace YamlSettingsServer.Tests
{
    public class Tests
    {


        [Test]
        public void TestParsingKeyValue()
        {
            var src = "key1=value1\nkey2=value2";

            var kv = src.ParseKeyValue();
            
            Assert.AreEqual("value1", kv["key1"]);
            Assert.AreEqual("value2", kv["key2"]);

        }
    }
}