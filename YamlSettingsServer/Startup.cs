using System;
using System.Text;
using DotNetCoreDecorators;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.WindowsAzure.Storage;
using MyYamlSettingsParser;

namespace YamlSettingsServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            YamlProcessor.SettingsModel = SettingsReader.ReadSettings();
        }
        
        private static readonly byte[] isAliveContent = Encoding.UTF8.GetBytes("{\"Result\":\"OK\"}") ;

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.Use(async (ctx, next) =>
            {

                
                if (ctx.Request.Path.Value == "/api/isalive")
                {
                    ctx.Response.ContentType = "application/json";
                    await ctx.Response.Body.WriteAsync(isAliveContent);
                    return;
                }

                try
                {
                    var yamlTemplate = await YamlProcessor.ReadYamlFromFileAsync();

                    var yamlTemplateLines = yamlTemplate.ParseYaml().AsReadOnlyList();

                    yamlTemplateLines = ctx.GetYamlAccordingToRequest(yamlTemplateLines).AsReadOnlyList();

                    if (yamlTemplateLines.Count == 0)
                    {
                        ctx.Response.StatusCode = 404;
                        await ctx.Response.WriteAsync("Not Found\n");
                        return;
                    }

                    var cloudAccount = CloudStorageAccount.Parse(YamlProcessor.SettingsModel.ConnectionString);

                    var yamlKeyValue = await cloudAccount.ReadYamlBlobAsync();
                    var keyValues = yamlKeyValue.ParseYaml().GetKeyValues();

                    var responseYaml = YamlProcessor.ApplyYaml(yamlTemplateLines, keyValues);
                
                    ctx.Response.ContentType = "application/yaml";
                    await ctx.Response.WriteAsync(responseYaml);
                }
                catch (Exception e)
                {
                    ctx.Response.ContentType = "application/text";
                    await ctx.Response.WriteAsync(e.Message);
                    await ctx.Response.WriteAsync(e.StackTrace);
                }
                
            });
           
            app.UseRouting();
        }
    }
}