using System;

namespace YamlSettingsServer
{

    public class SettingsModel
    {
        public string ConnectionString { get; set; }
        public string Container { get; set; }
        public string SettingsKeyValueFile { get; set; }
        public string SettingsTemplateFile { get; set; }
    }
    
    public static class SettingsReader
    {

        private static string GetEnvVariable(string name)
        {
            var result = Environment.GetEnvironmentVariable(name);
            
            if (string.IsNullOrEmpty(result))
                throw new Exception($"Environment variable {name} is not specified");


            return result;
        }

        public static SettingsModel ReadSettings()
        {
             return new SettingsModel
             {
                 ConnectionString = GetEnvVariable("CONNECTION_STRING"),
                 Container = GetEnvVariable("CONTAINER_NAME"),
                 SettingsKeyValueFile = GetEnvVariable("SETTINGS_KEYVALUE_FILE"),
                 SettingsTemplateFile = GetEnvVariable("SETTINGS_TEMPLATE_FILE")
             };
        }
        
    }
}