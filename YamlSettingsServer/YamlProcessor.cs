using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage;
using MyYamlSettingsParser;

namespace YamlSettingsServer
{
    public static class YamlProcessor
    {
        
        public static  SettingsModel SettingsModel;
        
        
        public const string KeyValueField = "KeyValue";
        
        public static async Task<byte[]> ReadYamlFromFileAsync()
        {
            return await File.ReadAllBytesAsync(SettingsModel.SettingsTemplateFile);
        }
        
        public static async Task<byte[]> ReadYamlBlobAsync(this CloudStorageAccount cloudAccount)
        {
            var container = await cloudAccount.GetBlockBlobReferenceAsync(SettingsModel.Container);

            var blob = container.GetBlobReference(SettingsModel.SettingsKeyValueFile);

            var stream = new MemoryStream();

            await blob.DownloadToStreamAsync(stream);

            return stream.ToArray();
        }

        public static IDictionary<string, string> GetKeyValues(this IEnumerable<YamlLine> yamlLines)
        {

            var result = new Dictionary<string, string>();

            foreach (var yamlLine in yamlLines.Where(itm => itm.Keys.Length == 2 && itm.Keys[0] == KeyValueField))
            {
                var key = yamlLine.Keys[1];
                if (!result.ContainsKey(key))
                    result.Add(key, yamlLine.Value ?? "");
            }

            return result;
        }


        public static IEnumerable<YamlLine> GetYamlAccordingToRequest(this HttpContext ctx, IEnumerable<YamlLine> yamlLines)
        {
            var path = ctx.Request.Path.Value;

            var rootKey = string.Empty;
            
            foreach (var yamlLine in yamlLines.Where(itm => itm.Keys[0] != KeyValueField))
            {

                if (yamlLine.Keys.Length == 1)
                {
                    if (yamlLine.Value == path)
                    {
                        rootKey = yamlLine.Keys[0];
                        yield return new YamlLine(new[]{rootKey}, null);
                    }
                    else
                    {
                        rootKey = string.Empty;
                    }
                    
                    continue;
                }
                
                
                if (yamlLine.Keys[0] == rootKey)
                    yield return yamlLine;
            }
        }

        public static IDictionary<string, string> ParseKeyValue(this string src)
        {
            var result = new Dictionary<string, string>();
            
            var stringReader = new StringReader(src);
            
            var line = stringReader.ReadLine();


            while (line != null)
            {
                try
                {
                    var indexEq = line.IndexOf('=');
                
                    if (indexEq <0)
                        continue;

                    var key = line.Substring(0, indexEq);
                    var valueLen = line.Length - indexEq - 1;
                    
                    var value =  valueLen<=0 ? "" : line.Substring(indexEq+1, valueLen);
                    
                    result.Add(key, value);
                }
                finally
                {
                    line = stringReader.ReadLine();    
                }

            }
            
            return result;

        }


        public static string ApplyYaml(IEnumerable<YamlLine> yamlLines, IDictionary<string, string> keyValues)
        {

            var result = new StringBuilder();

            foreach (var yamlLine in yamlLines)
            {
                if (yamlLine.Keys.Length > 1)
                    result.Append(new string(' ', yamlLine.Keys.Length - 1));


                var currentKey = yamlLine.Keys[^1];

                result.Append(currentKey);

                if (string.IsNullOrEmpty(yamlLine.Value))
                {
                    result.AppendLine(!currentKey.StartsWith('-') ? ":" : "");
                }
                else
                    result.AppendLine(": " + yamlLine.Value);
            }

            var resultYaml = result.ToString();
            
            foreach (var (key, value) in keyValues)
                resultYaml = resultYaml.Replace("${" + key + "}", value);

            return resultYaml;
        }
        
        
    }
}